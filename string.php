<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>string php dasar</title>
    </head>
    <body>

        <h1>Berlatih String</h1>
        <?php 
    echo "<h3>No 1</h3>";
    $kalimat1 = "PHP is never old";
    echo "Kalimat 1 :" . $kalimat1."<br>";
    echo "Panjang string : ".strlen($kalimat1)."<br>";
    echo "J8mlah kata : ".str_word_count($kalimat1)."<br>";
    
    echo "<h3>No 2</h3>";
    $kalimat2 = "I love PHP";
    echo "Kalimat 2 :" . $kalimat2 ."<br>";
    echo "kata 1 kalimat 2 : ".substr($kalimat2,0,6)."<br>";
    echo "kata 2 kalimat 2 : ".substr($kalimat2,7,9)."<br>";


    echo "<h3>No 3</h3>";
    $kalimat3 = "PHP is old but Good!";
    echo "Kalimat 3 :" . $kalimat3."<br>";
    echo "Kalimat 3 Ubah string : " . str_replace("Good!","awesome",$kalimat3);
    ?>

    </body>
</html>